Hephaesta/Yii2 language
==============
Module Hephaesta/Yii2 that helps in organizing multilingual

Installation
------------
The preferred way to install this extension is through [composer](http://getcomposer.org/download/).
Either run
```
composer require --prefer-dist delagics/hephaesta-yii2-lang "*"
```
or add
```
"delagics/hephaesta-yii2-lang": "*"
```
and do not forget to add repository information above:
```
"repositories": [
        {
            "type": "git",
            "url": "https://delagics@bitbucket.org/delagics/hephaesta-yii2-lang.git"
        }
],
```
to the require section of your `composer.json` file.

After that apply migration with console commands:
```
php yii migrate --migrationPath=@delagics/lang/console/migrations
```
Migration creates only one simple table with this structure and content:


| id | url | locale | name | is_default |
|----|-----|--------|------------|------------|
| 1 | uk | uk-UA | Українська | 1 |
| 2 | ru | ru-RU | Русский | 0 |
| 3 | en | en-US | English | 0 |


Usage
-----

Once the extension is installed and migration is apllied, simply use it in your code by setting `common\config\main.php`:
```php
'components'=>[

    'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'class'=>'delagics\lang\components\LanguageUrlManager',
        'rules'=>[
            '/' => 'site/index',
            '<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
        ],
    ],

    'request' => [
        'class' => 'delagics\lang\components\LanguageRequest'
    ],
],
```

You can use Language Widget that allows to switch between available languages:
```php
use delagics\lang\widgets\LanguageWidget;

echo LanguageWidget::widget();
```