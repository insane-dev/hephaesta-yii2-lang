<?php

use yii\db\Schema;
use yii\db\Migration;

class m150225_122930_language extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%language}}', [
            'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY',
            'url' => Schema::TYPE_STRING . '(3) NOT NULL',
            'locale' => Schema::TYPE_STRING . '(15) NOT NULL',
            'name' => Schema::TYPE_STRING . '(30) NOT NULL',
            'is_default' => Schema::TYPE_SMALLINT . '(2) UNSIGNED NOT NULL DEFAULT 0'
        ], $tableOptions);

        $this->createIndex('url', '{{%language}}', 'url', true);

        $this->batchInsert('language', ['url', 'locale', 'name', 'is_default'], [
            ['uk', 'uk-UA', 'Українська', 1],
            ['ru', 'ru-RU', 'Русский', 0],
            ['en', 'en-US', 'English', 0],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%language}}');
    }
}
