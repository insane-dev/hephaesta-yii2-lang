<?php

namespace delagics\lang\models;

use Yii;

/**
 * This is the model class for table "{{%language}}".
 *
 * @property integer $id
 * @property string $url
 * @property string $locale
 * @property string $name
 * @property integer $is_default
 */
class Language extends \yii\db\ActiveRecord
{
    public static $current = null;
    public static $languages;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%language}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'locale', 'name'], 'required'],
            [['is_default'], 'integer'],
            [['url', 'locale', 'name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'locale' => Yii::t('app', 'Locale'),
            'name' => Yii::t('app', 'Name'),
            'is_default' => Yii::t('app', 'is_default')
        ];
    }

    /**
     * Get all languages stored in "{{%language}}" table
     * @return array of laguages objects
     */
    public static function getLanguages()
    {
        if (empty(self::$languages)) {
            return self::$languages = self::find()->all();
        } else {
            return self::$languages;
        }

    }

    /**
     * Get the current language object
     * @return object Current laguage object
     */
    public static function getCurrent()
    {
        if (self::$current === null) {
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

    /**
     * Set application language to current language (locale)
     * @param string $url language short name
     * e.g.: 'uk' will set Yii::$app->language to 'uk-UA'
     */
    public static function setCurrent($url = null)
    {
        $language = self::getLangByUrl($url);
        self::$current = ($language === null) ? self::getDefaultLang() : $language;
        Yii::$app->language = self::$current->locale;
    }

    /**
     * Get default language object
     * @return object Default language
     */
    public static function getDefaultLang()
    {
        foreach (self::getLanguages() as $lang) {
            if ($lang->is_default == 1) {
                return $lang;
            }
        }
    }

    /**
     * Get language object by alphabetic identifier
     * @param  string $url
     * @return object|null language object
     */
    public static function getLangByUrl($url = null)
    {
        if ($url === null || empty($url)) {
            return null;
        } else {
            foreach (self::getLanguages() as $lang) {
                if ($lang->url == $url) {
                    return $lang;
                }
            }
            return null;
        }
    }

    /**
     * Get list of language objects without current language
     * @return array of language objects
     */
    public static function getLangsWOCurrent()
    {
        if (!empty(self::$languages) && !empty(self::$current)) {
            $langs = [];
            foreach (self::$languages as $lang) {
                if (self::$current->id != $lang->id) {
                    $langs[] = $lang;
                }
            }
            return $langs;
        }
    }
}