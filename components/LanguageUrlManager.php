<?php

namespace delagics\lang\components;

use yii\web\UrlManager;
use delagics\lang\models\Language;

class LanguageUrlManager extends UrlManager
{
    /**
     * Creates URL with language param, if specified
     * @param  array $params URL params
     * @return string url
     */
    public function createUrl($params)
    {
        if (isset($params['lang'])) {
            $lang = Language::getLangByUrl($params['lang']);
            if ($lang === null) {
                $lang = Language::getDefaultLang();
            }
            unset($params['lang']);
        } else {
            $lang = Language::getCurrent();
        }

        $url = parent::createUrl($params);

        if ($url == '/') {
            return '/'.$lang->url;
        } else {
            return '/'.$lang->url.$url;
        }
    }
}