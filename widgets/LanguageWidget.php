<?php

namespace delagics\lang\widgets;
use delagics\lang\models\Language;

class LanguageWidget extends \yii\bootstrap\Widget
{
    public function run()
    {
        return $this->render('index', [
            'currentLangName' => Language::$current->url,
            'langs' => Language::getLangsWOCurrent(),
        ]);
    }
}