<ul class="lang-switch col-sm-4 list-inline text-right">
    <li class="active"><a href="#"><?= $currentLangName;?></a></li>
    <?php foreach ($langs as $lang):?>
    <li role="presentation">
        <?= yii\helpers\Html::a($lang->url, '/'.$lang->url.Yii::$app->getRequest()->getLangUrl(), ['role'=>'menuitem', 'tabindex'=>'-1']) ?>
    </li>
    <?php endforeach; ?>
</ul>